# Linters

Linting tools that can be added individually the pipeline based on the types of files in a project.

For information on setting up pipelines see the [Tools README](https://gitlab.com/LibreFoodPantry/common-services/tools/gitlab-profile/-/blob/main/README.md).

For information about individual linters, see the README for that linter.